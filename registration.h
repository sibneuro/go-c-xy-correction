#ifndef REGISTRATION_H
#define REGISTRATION_H

namespace cv { class Mat; }

namespace sn {

    class TransformParameters;

    TransformParameters sibneuroCorrectParameters( const cv::Mat& before
                                                 , const cv::Mat& after);

    void remapMat( const cv::Mat& source
                 , cv::Mat& remapped
                 , const TransformParameters&);

    bool deltaIsSmallEnough(const cv::Mat& deltas);
    bool deltaIsSmallEnough3D(const cv::Mat& deltas);

namespace detail {

cv::Mat xDerivative(const cv::Mat& source);
cv::Mat yDerivative(const cv::Mat& source);

} // namespace detail;

} // namespace sn;

#endif // REGISTRATION_H
