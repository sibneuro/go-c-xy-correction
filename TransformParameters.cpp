#include "TransformParameters.h"

namespace sn {

TransformParameters::TransformParameters()
    : xShift(0.)
    , yShift(0.)
    , zShift(0.)
    , angleDeg(0.)
{}

TransformParameters::TransformParameters( const double x
                                        , const double y
                                        , const double z
                                        , const double angle)
    : xShift(x)
    , yShift(y)
    , zShift(z)
    , angleDeg(angle)
{}

void TransformParameters::subtractDelta( const double dx
                                       , const double dy
                                       , const double dz
                                       , const double dAngle )
{
    xShift   += dx;
    yShift   += dy;
    zShift   += dz;
    angleDeg -= dAngle * 180. / 3.14159265359;
}

double TransformParameters::getXShift() const { return xShift; }
double TransformParameters::getYShift() const { return yShift; }
double TransformParameters::getZShift() const { return zShift; }
double TransformParameters::getAngle()  const { return angleDeg; }

void TransformParameters::setXShift(const double x) { xShift = x; }
void TransformParameters::setYShift(const double y) { yShift = y; }
void TransformParameters::setZShift(const double z) { zShift = z; }
void TransformParameters::setAngle (const double angle) { angleDeg = angle; }

} // namespace sn;
