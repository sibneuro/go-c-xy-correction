#ifndef TRANSFORMPARAMETERS_H
#define TRANSFORMPARAMETERS_H

namespace sn { class TransformParameters; }

class QTransform;
class QDebug;

class sn::TransformParameters {
public:

    TransformParameters();
    TransformParameters(double x, double y, double z, double angle);

    /** Subtracts deltas from TransformParameters values.
     * @param dx, dy, dz coordinate deltas.
     * @param dAngle angle delta in radians. */
    void subtractDelta(double dx, double dy, double dz, double dAngle);

    double getXShift() const;
    double getYShift() const;
    double getZShift() const;
    double getAngle() const;

    void setXShift(double x);
    void setYShift(double y);
    void setZShift(double z);
    /** Sets transformation angle.
     * @param angle in degrees, counter-clockwise. */
    void setAngle(double angle);

private:

    double xShift, yShift, zShift;
    double angleDeg; ///< counter-clockwise, degrees

};

#endif // TRANSFORMPARAMETERS_H
