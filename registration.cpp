#include "registration.h"
#include "TransformParameters.h"

#include <opencv2/imgproc/imgproc.hpp>

#include <cmath>

namespace sn {

TransformParameters sibneuroCorrectParameters( const cv::Mat& templateMat
                                             , const cv::Mat& referenceMat )
{
    /* It is Inverse Compositional Algorithm.
     * We use rigid transformation and intensity-based measure.
     * For minimization of measure we use Gauss-Newton method.
     *
     * Description: https://yadi.sk/i/TieIe867r5WgQ
     */

    using namespace cv;

    // Algorithm may be divided on two parts:
    // 0 - Precomputation part.
    // 1 - Main loop.

    // Precomputation part.

    // 0.0. Initial approximation.
    TransformParameters parameters;
    Mat deltaParameters;

    // 0.1. Calculate gradient of Template Image.
    const Mat templateGradX = detail::xDerivative(templateMat);
    const Mat templateGradY = detail::yDerivative(templateMat);

    auto makeJ = [](const Mat& gradX, const Mat& gradY) -> Mat {

        // 0.2. Calculate Jacobian matrix of Rigid Transformation.
        //      In our case we simply compute grids for inner product.

        auto makeGridX = [](const cv::Size& size) -> Mat {
            cv::Mat grid(size, CV_64F);
            for (int i = 0; i < size.height; ++i)
            for (int j = 0; j < size.width ; ++j) {
                grid.at<double>(i, j) = j;
            }
            return grid;
        };
        auto makeGridY = [](const cv::Size& size) -> Mat {
            cv::Mat grid(size, CV_64F);
            for (int i = 0; i < size.height; ++i)
            for (int j = 0; j < size.width ; ++j) {
                grid.at<double>(i, j) = i;
            }
            return grid;
        };

        const Size size = gradX.size();
        const Mat gridX = makeGridX(size), gridY = makeGridY(size);

        // 0.3. Calculate product of gradient and jacobian matrix in all points.
        Mat gradXgridY(size, CV_64F), gradYgridX(size, CV_64F);
        multiply(gridY, gradX, gradXgridY);
        multiply(gridX, gradY, gradYgridX);

        Mat result(size, CV_64F);
        subtract(gradYgridX, gradXgridY, result);

        return result;
    };

    const Mat J = makeJ(templateGradX, templateGradY);

    // 0.4. Last step of precomputation part.
    //      We compute Hessian matrix.

    auto makeA = [](const Mat& templateGradX, const Mat& templateGradY, const Mat& J) -> Mat {
        Mat A(3, 3, CV_64F);

        Mat mult(templateGradX.size(), CV_64F);
        multiply(templateGradX, templateGradX, mult);
        A.at<double>(0, 0) = sum(mult)[0];

        multiply(templateGradX, templateGradY, mult);
        A.at<double>(0, 1) = sum(mult)[0];

        multiply(templateGradX, J, mult);
        A.at<double>(0, 2) = sum(mult)[0];

        A.at<double>(1, 0) = A.at<double>(0, 1);

        multiply(templateGradY, templateGradY, mult);
        A.at<double>(1, 1) = sum(mult)[0];

        multiply(templateGradY, J, mult);
        A.at<double>(1, 2) = sum(mult)[0];

        A.at<double>(2, 0) = A.at<double>(0, 2);

        A.at<double>(2, 1) = A.at<double>(1, 2);

        multiply(J, J, mult);
        A.at<double>(2, 2) = sum(mult)[0];

        return A;
    };
    const Mat A = makeA(templateGradX, templateGradY, J);

    // Part two. Main loop.

    Mat remapped(referenceMat.size(), referenceMat.type());

    int steps = 0;
    static const int MAX_STEPS = 500;
    do {
        // 1. Remap Reference Image to Template.
        //    We use rigid transformation with current parameter p.
        remapMat(referenceMat, remapped, parameters);

        // 2. Calculate e - difference (error)
        //    between transformed Reference and invariant Template Image.
        subtract(remapped, templateMat, remapped);
        const Mat& e = remapped;

        // 3. Calculate Gradient-Weighted Resudial Vector b.
        Mat b(3, 1, CV_64F);

        Mat mult(templateGradX.size(), CV_64F);
        multiply(e, templateGradX, mult);
        b.at<double>(0) = sum(mult)[0];

        multiply(e, templateGradY, mult);
        b.at<double>(1) = sum(mult)[0];

        multiply(e, J, mult);
        b.at<double>(2) = sum(mult)[0];

        // 4. By solving linear system we obtain increment of parameters vector dP.
        solve(A, b, deltaParameters);
        deltaParameters.at<double>(0) *= 10;
        deltaParameters.at<double>(1) *= 10;
        deltaParameters.at<double>(2) *= 10;

        // 5. Update parameters vector P.
        parameters.subtractDelta( deltaParameters.at<double>(0)
                                , deltaParameters.at<double>(1)
                                , 0. // dz
                                , deltaParameters.at<double>(2) );

        ++steps;
        if (steps > MAX_STEPS) {
            return TransformParameters();
        }
    } while (not deltaIsSmallEnough(deltaParameters)); // 6. Check for convergence.

    return parameters;
}

void remapMat( const cv::Mat& source
             , cv::Mat& remapped
             , const TransformParameters& p )
{
    using namespace cv;
    Mat rotationMat = getRotationMatrix2D(Point2f(source.cols / 2, source.rows / 2), p.getAngle(), 1.);
    rotationMat.at<double>(0, 2) += p.getXShift();
    rotationMat.at<double>(1, 2) += p.getYShift();

    warpAffine(source, remapped, rotationMat, source.size());
}

bool deltaIsSmallEnough(const cv::Mat& deltas)
{
    return ( std::abs(deltas.at<double>(0)) < 0.01
          && std::abs(deltas.at<double>(1)) < 0.01
          && std::abs(deltas.at<double>(2)) < 0.01 );
}

bool deltaIsSmallEnough3D(const cv::Mat& deltas)
{
    return ( std::abs(deltas.at<double>(0)) < 0.1
          && std::abs(deltas.at<double>(1)) < 0.1
          && std::abs(deltas.at<double>(2)) < 0.1
          && std::abs(deltas.at<double>(3)) < 0.1 );
}

namespace detail {

cv::Mat xDerivative(const cv::Mat& source)
{
    cv::Mat grad64F;
    cv::Scharr(source, grad64F, CV_64F, 1, 0);
    return grad64F;
}

cv::Mat yDerivative(const cv::Mat& source)
{
    cv::Mat grad64F;
    cv::Scharr(source, grad64F, CV_64F, 0, 1);
    return grad64F;
}

} // namespace detail;

} // namespace sn;
