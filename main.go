package main

// import and link to C++ code and libraries

// #cgo CXXFLAGS: -std=c++11
// #cgo LDFLAGS: -Llibs -lopencv_core -lopencv_highgui -lopencv_imgproc
// void calculateCorrectionParameters(char* filename1, char* filename2);
import "C"

import "fmt"
import "os"

func main() {
    args := os.Args[1:] // command line arguments
    nArgs := len(args)

    if (nArgs == 2) {
        // call of C++ interface function with translated strings.
        C.calculateCorrectionParameters(C.CString(args[0]), C.CString(args[1]))
    } else {
        fmt.Printf("usage: smartDiffGo imageOne.jpg imageTwo.jpg");
    }
}
