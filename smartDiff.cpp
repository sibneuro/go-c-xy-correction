/* This file is compatibility layer between Go and C++ code.
 * Interface functions are defined here.
 * All C++ code should be wrapped in extern "C". */

#include <cstdlib>
#include <iostream>
#include <memory>

#include "registration.h"
#include "TransformParameters.h"

#include <opencv2/highgui/highgui.hpp>

extern "C" {

void calculateCorrectionParameters(char* const filename1, char* const filename2)
{
    using namespace std;
    // filename1 and filename2 arrays must be freed by this function.
    unique_ptr<char, default_delete<char[]>> _filename1(filename1);
    unique_ptr<char, default_delete<char[]>> _filename2(filename2);

    cout << "filename 1: " << filename1 << endl
         << "filename 2: " << filename2 << endl;

    cv::Mat imageBefore = cv::imread(filename1, CV_LOAD_IMAGE_GRAYSCALE);
    if (imageBefore.data == nullptr) {
        cout << "First image could not be read!" << endl;
        return;
    }
    cv::Mat imageAfter  = cv::imread(filename2, CV_LOAD_IMAGE_GRAYSCALE);
    if (imageAfter.data == nullptr) {
        cout << "Second image could not be read!" << endl;
        return;
    }

    cv::Mat beforeFloatMat, afterFloatMat;
    imageBefore.convertTo(beforeFloatMat, CV_64F);
    imageAfter .convertTo( afterFloatMat, CV_64F);
    sn::TransformParameters transformParameters =
        sn::sibneuroCorrectParameters(beforeFloatMat, afterFloatMat);

    cout << "XYZ shifts: "
         << transformParameters.getXShift() << ", "
         << transformParameters.getYShift() << ", "
         << transformParameters.getZShift() << ", "
         << "Angle (deg):"
         << transformParameters.getAngle()
         << endl;
}

} // extern "C"
